import React, {useState, useEffect} from 'react';

import CartItem from './CartItem';
import { useCart } from '../../hooks/useCart';
import styles from './CartProducts.module.scss';

const CartProducts = () => {

    const { cartItems } = useCart();
    const fetchData = () => {
        return fetch("https://randomuser.me/api/")
              .then((response) => response.json())
              .then((data) => console.log(data));}
    console.log(fetchData);
    let data = require('../../data.json');
    const [d, setD] = useState('');
    useEffect(() => {
    setD(data[0]);
    });

    return (
        <div className={styles.p__container}>
            <div className="card card-body border-0">

                {
                    cartItems.map(product =>  <CartItem key={product.id} product={product}/>)
                }

            </div>
        </div>

     );
}

export default CartProducts;
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import CartProducts from './CartProducts';
import Layout from '../../components/Layout';
import { useCart } from '../../hooks/useCart';
import { formatNumber } from '../../helpers/utils';
import axios from "axios"
import { Row, Col, Tabs, Card } from "antd";
import 'antd/dist/antd.css';
import CartItem from './CartItem';

const Cart = () => {

    let data = require('../../data.json');
    const [d, setD] = useState('')

    useEffect(() => {
        setD(data[0]);
        const result = axios.get("https://run.mocky.io/v3/a67edc87-49c7-4822-9cb4-e2ef94cb3099");
        console.log(result);
    });

    const { TabPane } = Tabs;
    console.log(d);

    const { total, cartItems, itemCount, clearCart, checkout, handleCheckout } = useCart();

    return (
        <Layout title="Cart" description="This is the Cart page" >
            <div >
                {d.branch_name}
                {/* <div className="text-center mt-5">
                    <h1>Cart</h1>
                    <p>This is the Cart Page.</p>
                </div> */}
                <div>
                    <Tabs defaultActiveKey="1">
                        {d.table_menu_list?.map((menu_category) => (
                            <TabPane tab={menu_category.menu_category} key={menu_category.menu_category_id}>
                                <div className="card card-body border-0">

                                    {
                                        menu_category.category_dishes?.map(product => <CartItem key={product.id} product={product} />)
                                    }

                                </div>
                            </TabPane>
                        ))}
                    </Tabs>
                </div>
                {/* <div className="row no-gutters justify-content-center">
                    <div className="col-sm-9 p-3">
                        {
                            cartItems.length > 0 ?
                                <CartProducts /> :
                                <div className="p-3 text-center text-muted">
                                    Your cart is empty
                                </div>
                        }

                        {checkout &&
                            <div className="p-3 text-center text-success">
                                <p>Checkout successfull</p>
                                <Link to="/" className="btn btn-outline-success btn-sm">BUY MORE</Link>
                            </div>
                        }
                    </div>
                    {
                        cartItems.length > 0 &&
                        <div className="col-sm-3 p-3">
                            <div className="card card-body">
                                <p className="mb-1">Total Items</p>
                                <h4 className=" mb-3 txt-right">{itemCount}</h4>
                                <p className="mb-1">Total Payment</p>
                                <h3 className="m-0 txt-right">{formatNumber(total)}</h3>
                                <hr className="my-4" />
                                <div className="text-center">
                                    <button type="button" className="btn btn-primary mb-2" onClick={handleCheckout}>CHECKOUT</button>
                                    <button type="button" className="btn btn-outlineprimary btn-sm" onClick={clearCart}>CLEAR</button>
                                </div>

                            </div>
                        </div>
                    }

                </div> */}
            </div>
        </Layout>
    );
}

export default Cart;
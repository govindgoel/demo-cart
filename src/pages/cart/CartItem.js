import React, { useState } from 'react';

import { useCart } from '../../hooks/useCart';
import { formatNumber } from '../../helpers/utils';
import { PlusCircleIcon, MinusCircleIcon, TrashIcon } from '../../components/icons';
import { Card } from "antd";
const CartItem = ({ product }) => {

    const { increase, decrease, removeProduct } = useCart();

    let [count, setCount] = useState(0);

    function incrementCount() {
        count = count + 1;
        setCount(count);
        if (window.localStorage.getItem("cartItems")) {
            let x = window.localStorage.getItem("cartItems");
            console.log(x, count);
            let x1 = Number(x) + Number(1);
            console.log(x1);
            window.localStorage.setItem("cartItems", x1);
        }
        else {
            window.localStorage.setItem("cartItems", 0);
        }
    }
    function decrementCount() {
        if (count > 0) {
            count = count - 1;
            setCount(count);
        }
    }
    return (
        <Card>
            <div className="row no-gutters">
                <div className="col-sm-8">
                    <h5 className="mb-1">{product.dish_name}</h5>
                    <p className="mb-1">{product.dish_currency}: {formatNumber(product.dish_price)} </p>
                    <p className="mb-1">{product.dish_description}</p>
                    <button onClick={incrementCount}>+</button>
                    {' '}{count}{' '}
                    <button onClick={decrementCount}>-</button>
                    {product.addonCat.length > 0 ? (<p style={{ color: "red" }}>customization available</p>) : null}
                </div>
                <div className="col-sm-2 text-center ">
                    <p className="mb-0">{product.dish_calories} calories</p>
                </div>
                <div className="col-sm-2">
                    <img
                        alt={product.dish_name}
                        style={{ margin: "0 auto", maxHeight: "100px", maxWidth: "100px", borderRadius: "5px" }}
                        src={product.dish_image} className="img-fluid d-block" />
                </div>
                {/* <div className="col-sm-4 p-2 text-right">
                    <button
                        onClick={() => increase(product)}
                        className="btn btn-primary btn-sm mr-2 mb-1">
                        <PlusCircleIcon width={"20px"} />
                    </button>

                    {
                        product.quantity > 1 &&
                        <button
                            onClick={() => decrease(product)}
                            className="btn btn-danger btn-sm mb-1">
                            <MinusCircleIcon width={"20px"} />
                        </button>
                    }

                    {
                        product.quantity === 1 &&
                        <button
                            onClick={() => removeProduct(product)}
                            className="btn btn-danger btn-sm mb-1">
                            <TrashIcon width={"20px"} />
                        </button>
                    }

                </div> */}
            </div>
        </Card>
    );
}

export default CartItem;